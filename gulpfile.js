'use strict';

var gulp = require('gulp');
var wrench = require('wrench');
var browserSync = require('browser-sync').create();

wrench.readdirSyncRecursive('./gulp').filter(function(file) {
    return (/\.js$/i).test(file);
}).map(function(file) {
    require('./gulp/' + file);
});

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'main-bower-files']
});