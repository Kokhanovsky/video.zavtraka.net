'use strict';

var path = require('path');
var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

var $ = require('gulp-load-plugins')();

gulp.task('sass', function () {
    gulp.src(['./public/sass/styles.sass', './public/sass/styles-bourbon.sass'])
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', errorHandler))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./public/css'));
});

// Handle the error
function errorHandler (error) {
    console.log(error.toString());
    this.emit('end');
}