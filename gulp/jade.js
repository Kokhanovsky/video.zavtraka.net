'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')();

gulp.task('jade', function() {
    gulp.src(['./public/jade/**/index.jade'])
        .pipe($.jade({
            pretty: true
        }))
        .pipe(gulp.dest('./public'))
    ;
});