'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync').create();


gulp.task('serve', function() {

    browserSync.init(["public/css/*.css", "public/**/*.html"], {
        proxy: "video.zavtrak.dev"
    });

    gulp.watch("public/jade/**/*.jade", ['jade']);
    gulp.watch("public/sass/**/*.sass", ['sass']);
    gulp.watch("public/sass/**/*.scss", ['sass']);
    gulp.watch('public/ts/**/*.ts', ['scripts']).on('change', browserSync.reload);


});