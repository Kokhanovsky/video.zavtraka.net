'use strict';

var gulp = require('gulp');

var $ = require('gulp-load-plugins')();

// compile typescript
gulp.task('scripts', function () {
    var tsResult = gulp.src('public/ts/references.ts')
        .pipe($.sourcemaps.init())
        .pipe($.typescript({
            noImplicitAny: true,
            out: 'app.min.js'
        }));
    return tsResult.js
        //.pipe($.uglify())
        .pipe($.sourcemaps.write('.', {includeContent: false, sourceRoot: '/ts'}))
        .pipe(gulp.dest('public/js/'));
});