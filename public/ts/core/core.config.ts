module app.core {
    'use strict';

    export interface IYT {
        forUsername: string,
        channelId: string,
        key: string,
        callback: string
    }

    export interface IConfigService {
        api: string;
        YT: IYT
    }

    export class configService implements IConfigService {

        public YT: IYT = {
            forUsername: "zavtrakanet",
            channelId: "UClN9BQsKbhsj-eSbKlWpIjA",
            key: "AIzaSyCigaQsG-VbUIvFpfvbZ48BI1eVhZnoH1A",
            callback: "JSON_CALLBACK"
        };
        
        public api: string = "/api";

        constructor() { }

    }

    angular
        .module('app.core')
        .service('app.core.configService', configService);
}