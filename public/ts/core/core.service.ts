module app.core {
    'use strict';

    export interface ICoreService {
        $http: ng.IHttpService;
        $log: ng.ILogService;
        $timeout: ng.ITimeoutService;
        $window: ng.IWindowService;
    }

    export class commonService implements ICoreService {
        static $inject = ['$http', '$log', '$timeout', '$window'];
        constructor(
            public $http: ng.IHttpService,
            public $log: ng.ILogService,
            public $timeout: ng.ITimeoutService,
            public $window: ng.IWindowService
        ) {}

    }

    angular
        .module('app.core')
        .service('app.core.commonService', commonService);
}