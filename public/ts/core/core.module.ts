((): void => {
    'use strict';

    angular.module('app.core', [
        "duScroll",
        "restangular",
        "ngSanitize",
        "youtube-embed",
        "angularMoment",
        "timer"
    ]).run(appRun);

    appRun.$inject = ["amMoment"];
    function appRun(amMoment: any) {
        amMoment.changeLocale('ru');
    }

})();