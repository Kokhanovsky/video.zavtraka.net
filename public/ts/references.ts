// TYPINGS

/// <reference path="../../typings/angularjs/angular.d.ts" />
/// <reference path="../../typings/jquery/jquery.d.ts" />
/// <reference path="../../typings/angular-scroll/angular-scroll.d.ts" />
/// <reference path="../../typings/angularjs/angular-route.d.ts" />
/// <reference path="../../typings/restangular/restangular.d.ts" />
/// <reference path="../../typings/moment/moment-node.d.ts" />
/// <reference path="../../typings/moment/moment.d.ts" />

// APP

/// <reference path="app.module.ts" />

// CORE

/// <reference path="core/core.module.ts" />
/// <reference path="core/core.service.ts" />
/// <reference path="core/core.config.ts" />


// VIDEO

/// <reference path="video/video.module.ts" />
/// <reference path="video/video.controller.ts" />
/// <reference path="video/video.service.ts" />


// DIRECTIVES

/// <reference path="directives/directives.module.ts" />
/// <reference path="directives/directives.factory.ts" />
/// <reference path="directives/rectangle.directive.ts" />
/// <reference path="directives/background-image.directive.ts" />
/// <reference path="directives/custom-placeholder.directive.ts" />
/// <reference path="directives/owl.directive.ts" />

/// <reference path="animations.ts" />
