((): void => {
    "use strict";

    angular.module("app", [
        "app.core",
        "app.directives",
        "app.video"
    ]);

})();