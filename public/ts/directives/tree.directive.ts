module app.directives {
    import IScope = angular.IScope;
    'use strict';

    export class tree implements ng.IDirective {

        static $inject = ["$compile"];

        constructor(private $compile: ng.ICompileService) {
        }

        public template: string = '<div data-ng-include="templateUrl"></div>';

        public scope = {
            tree: "=",
            templateUrl: "@"
        };

        // http://plnkr.co/edit/JAIyolmqPqO9KsynSiZp
        public compile (element: ng.IAugmentedJQuery, link: ng.IDirectivePrePost): ng.IDirectivePrePost {
            var self = this;
            // Break the recursion loop by removing the contents
            var contents = element.contents().remove();
            var compiledContents: Function;
            return {
                pre: (link && link.pre) ? link.pre : null,
                /**
                 * Compiles and re-adds the contents
                 */
                post: function(scope: ng.IScope, element: ng.IAugmentedJQuery){
                    // Compile the contents
                    if(!compiledContents){
                        compiledContents = self.$compile(contents);
                    }
                    // Re-add the compiled contents to the element
                    compiledContents(scope, (clone: ng.IAugmentedJQuery) => {
                        element.append(clone);
                    });
                    // Call the post-linking function, if any
                    if(link && link.post){
                        link.post.apply(null, arguments);
                    }
                }
            };
        }

    }

    angular.module('app.directives')
        .directive("tree", app.directives.getFactoryFor(tree));

}