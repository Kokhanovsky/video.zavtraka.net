module app.directives {
    'use strict';

    interface IRectantgleAttrs extends ng.IAttributes {
        rectangle: number;
        background: string;
    }

    export class rectangle implements ng.IDirective {

        public link(scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: IRectantgleAttrs): void {
            if (attrs.background) {
                $(element).css({
                    "background-image": "url(" + attrs.background + ")"
                });
            }
            var getHeight = function (w: number) {
                var k = attrs.rectangle ? attrs.rectangle : 1;
                return Math.ceil(w / k);
            };
            scope.$watch(function () { return element.width(); }, function (width) {
                if (!width)
                    return;
                $(element).height(Math.floor(getHeight(width)));
                //$(element).width($(element).width() + 0.1);
            });
            $(window).on("resize", function () {
                if (!$(element).width())
                    return;
                $(element).height(Math.floor(getHeight($(element).width())));
                //$(element).width($(element).width() + 0.1);
            });

        }
    }

    angular.module('app.directives')
        .directive("rectangle", app.directives.getFactoryFor(rectangle));

}