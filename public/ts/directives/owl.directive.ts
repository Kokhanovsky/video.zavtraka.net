module app.directives {
    'use strict';

    interface IOwlAttrs extends ng.IAttributes {
        rectangle: number;
        background: string;
    }

    interface IOwl extends ng.IAugmentedJQuery {
        owlCarousel: any;
    }


    export class owl implements ng.IDirective {

        public link(scope: ng.IScope, element: IOwl, attrs: IOwlAttrs): void {
            element.owlCarousel({
                loop:true,
                margin:10,
                responsiveClass:true,
                responsive:{
                    0:{
                        items:1,
                        nav:true
                    },
                    600:{
                        items:2,
                        nav:false
                    },
                    1000:{
                        items:3,
                        nav:true,
                        loop:false
                    }
                }
            })

        }
    }

    angular.module('app.directives')
        .directive("owl", app.directives.getFactoryFor(owl));

}