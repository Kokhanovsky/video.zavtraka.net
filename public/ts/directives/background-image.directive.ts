module app.directives {
    'use strict';

    interface IBackgroundImageAttrs extends ng.IAttributes {
        backgroundImage: string;
    }

    export class backgroundImage implements ng.IDirective {

        public link(scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: IBackgroundImageAttrs): void {
            element.css("opacity", 0);
            $('<img/>').attr('src', attrs.backgroundImage).load(() => {
                element.css("opacity", 1);
            });
            $(element).css({
                "background-image": "url(" + attrs.backgroundImage + ")"
            });
        }

    }

    angular.module('app.directives')
        .directive("backgroundImage", app.directives.getFactoryFor(backgroundImage));

}