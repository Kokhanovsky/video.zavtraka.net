module app.directives {
    'use strict';

    interface ICustomPlaceholderAttrs extends ng.IAttributes {
        customPlaceholder: string;
    }

    export class customPlaceholder implements ng.IDirective {

        public link(scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ICustomPlaceholderAttrs): void {
            var cssClass = "custom-placeholder";
            var cssClassMove = "custom-placeholder-move";
            element.wrap("<div></div>");
            var wrapper = element.parent();
            wrapper.css({
                "position": "relative"
            });
            element.before("<span>"+attrs.customPlaceholder+"</span>");
            var placeholder = element.prev();
            placeholder.addClass(cssClass);
            var init = true;
            scope.$watch(() => { return element.val() }, (newVal, oldVal) => {
                if (element.prop("tagName") == "SELECT") {
                    if (newVal) {
                        placeholder.addClass(cssClassMove);
                    } else {
                        placeholder.removeClass(cssClassMove);
                    }
                } else if ((element.prop("tagName") == "INPUT") && init) {
                    if (newVal)
                        placeholder.addClass(cssClassMove);
                    else
                        placeholder.removeClass(cssClassMove);
                    init = false;
                }
            });
            placeholder.on("click", () => {
                element.focus();
            });
            element.on("focus", () => {
                placeholder.addClass(cssClassMove);
            });
            element.focusout(() => {
                if (!element.val())
                    placeholder.removeClass(cssClassMove);
            });
        }
    }

    angular.module('app.directives')
        .directive("customPlaceholder", app.directives.getFactoryFor(customPlaceholder));

}