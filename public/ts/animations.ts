var WOW: any;
new WOW().init();



    var $ = jQuery;
    setTimeout(()=>{
        $("ul.social-icons li a").each(function(index) {
            $(this).css({
                "transform": "translateY(-100px)",
            });
            setTimeout(() => {
                $(this).css({
                    "transform": "translateY(0)",
                    "opacity": 1
                });
                $(this).css('transform', '');
            }, 200 * index + 150);
        });
    },3000);

    function newVideoOffsetTop() {
        var hero = $("#hero");
        var header = $("header");
        var heroContent = $(".content", hero);
        if (!hero.length || !header.length || !heroContent.length)
            return;
        hero.css("visibility", "visible");
        heroContent.css({
            "padding-top": header.height() + header.offset().top +"px"
        });
    }

    newVideoOffsetTop();

    $(window).on("resize", () => {
        newVideoOffsetTop();
    });

    $(window).on("scroll", () => {
        var wScroll = $(this).scrollTop();
        $(".videos-bg").css({
            "background-position": "0 "+ wScroll*0.9 +"px"
        });
        $(".videos-bg-blur").css({
            "background-position": "0 "+ wScroll*0.3 +"px"
        });

        var aboutSection = $("#about");
        if ((wScroll + $(window).height()) > (aboutSection.offset().top)) {
            var offset = (wScroll + $(window).height()) - (aboutSection.offset().top);
            aboutSection.css({
                "background-position": "0 "+ (offset*0.5 + aboutSection.height()) + "px"
            });
        }

    });
