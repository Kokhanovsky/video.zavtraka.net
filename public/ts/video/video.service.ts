module app.video {
    "use strict";

    export interface IVideo {
        id: number,
        title: string;
        description: string;
        video_id: string;
        thumbnail: string;
        date: Date;
    }

    export interface ICategory {
        id: number;
        parent_id: number;
        depth: number;
        title: string;
        children: Array<ICategory>;
        videos_count: number;
    }

    export interface IVideoFilter {
        page: number,
        perPage: number,
        categoryId: number,
        order: string,
        q: string
    }

    export interface IChannelData {
        title: string,
        description: string,
        thumbnail: string,
        date: Date,
        videoCount: number,
        viewCount: number,
        subscriberCount: number
    }

    export interface IVideoService {
        getVideos(ctrl: IVideoController): void;
        getTopRated(ctrl: IVideoController): void;
        getCategories(ctrl: IVideoController): void;
        getCategoryById(categoryId: number): ICategory;
        getChannelData(controller: IVideoController): void;
        getNewVideo(controller: IVideoController): void;
        watchVideoFilter(controller: IVideoController): void;
        watchLocation(controller: IVideoController): void;
    }

    class VideoService implements IVideoService {


        public categories: Array<ICategory>;

        static $inject = ["app.core.configService", "Restangular", "$http"];

        constructor(
            private config: app.core.IConfigService,
            private restangular: restangular.IService,
            private $http: ng.IHttpService
        ) {
            restangular.setBaseUrl(config.api);
            restangular.setFullResponse(true);
        }

        getVideos(ctrl: IVideoController) {
            ctrl.videosLoading = true;
            this.restangular.all('video').getList(ctrl.videoFilter).then((response: any) => {
                var data = response.data.plain();
                for (var i = 0; i < data.length; i++){
                    data[i].date_add = moment(data[i].date_add).toDate();
                    ctrl.videos.push(response.data[i]);
                }
                ctrl.videosTotalCount = response.headers()["x-total-count"]
            }).finally(() => {
                ctrl.videosLoading = false;
            });
        }

        getTopRated(ctrl: IVideoController) {
            var yt = this.config.YT;
            return this.$http.jsonp("https://www.googleapis.com/youtube/v3/search",
                {
                    params: {
                        channelId: yt.channelId,
                        key: yt.key,
                        type: "video",
                        callback: yt.callback,
                        maxResults: 3,
                        part: "snippet",
                        order: "viewCount"
                    }
                }).then((response: any) => {
                var data: any = response.data.items;
                console.log(data);
                for (var i = 0; i < data.length; i++){
                    ctrl.topRated.push(<IVideo> {
                        title: data[i].snippet.title,
                        description: data[i].snippet.description,
                        video_id: data[i].id.videoId,
                        thumbnail: data[i].snippet.thumbnails.high.url,
                        date: moment(data[i].snippet.publishedAt).toDate()
                    });
                }
                //console.log(ctrl.topRated);
            });
        }

        getNewVideo(ctrl: IVideoController) {
            var yt = this.config.YT;
            return this.$http.jsonp("https://www.googleapis.com/youtube/v3/search",
                {
                    params: {
                        channelId: yt.channelId,
                        key: yt.key,
                        callback: yt.callback,
                        maxResults: 1,
                        part: "snippet",
                        order: "date"
                    }
                }).then((response: any) => {
                var d: any = response.data.items[0];
                ctrl.newVideo = <IVideo> {
                    video_id: d.id.videoId,
                    title: d.snippet.title,
                    description: d.snippet.description,
                    date: moment(d.snippet.publishedAt).toDate(),
                    thumbnail: d.snippet.thumbnails.high.url
                };
            });
        }

        getChannelData(ctrl: IVideoController) {
            var yt = this.config.YT;
            return this.$http.jsonp("https://www.googleapis.com/youtube/v3/channels",
            {
                params: {
                    forUsername: yt.forUsername,
                    key: yt.key,
                    callback: yt.callback,
                    part: "snippet,statistics"
                }
            }).then((response: any) => {
                var d: any = response.data.items[0];
                ctrl.channelData = <IChannelData> {
                    viewCount: d.statistics.viewCount,
                    subscriberCount: d.statistics.subscriberCount,
                    videoCount: d.statistics.videoCount,
                    title: d.snippet.title,
                    description: d.snippet.description,
                    thumbnail: d.snippet.thumbnails.high.url,
                    date: moment(d.snippet.publishedAt).toDate()
                };
            });
        }

        getCategories(ctrl: IVideoController) {
            this.restangular.all('video/categories').getList().then((response: any) => {
                this.categories = response.data.plain();
                ctrl.selectCategory = this.categoriesForSelect();
            });
        }

        getCategoryById(categoryId: number) {
            if (!this.categories)
                return null;
            for(var i = 0; i < this.categories.length; i++) {
                var category = this.categories[i];
                if (category.id == categoryId) {
                    return category;
                }
            }
            return null;
        }

        watchVideoFilter(ctrl: IVideoController) {
            var flag = false;
            ctrl.$scope.$watch(<any> angular.bind(ctrl, () => {
                return ctrl.videoFilter;
            }), (newVal: IVideoFilter, oldVal: IVideoFilter) => {
                if (flag) {
                    // ignore watch
                    flag = false;
                    return;
                }
                // if some filters are changed except page number
                if (newVal.page == oldVal.page) {
                    ctrl.videos = [];
                    if (newVal.page != 1) {
                        ctrl.videoFilter.page = 1;
                        // ignore next time
                        flag = true;
                    }
                }
                this.getVideos(ctrl);
                this.setUrlParams(ctrl, newVal);
            }, true);
        }

        watchLocation(ctrl: IVideoController) {
            this.setParamsFromLocation(ctrl);
            ctrl.$scope.$on('$locationChangeSuccess', () => {
                this.setParamsFromLocation(ctrl);
            });
        }

        private setUrlParams(ctrl: IVideoController, filter: IVideoFilter) {
            // if category exists with certain key then set location with categoryId key
            if (!this.categories || this.getCategoryById(filter.categoryId))
                ctrl.$location.search("categoryId", ctrl.videoFilter.categoryId);
            else {
                ctrl.$location.search("categoryId", null);
            }
            if (filter.q)
                ctrl.$location.search("q", ctrl.videoFilter.q);
            else {
                ctrl.$location.search("q", null);
            }
            // clear url
            if (!ctrl.videoFilter.categoryId && !ctrl.videoFilter.q)
                ctrl.$location.url(ctrl.$location.path());

        }
        
        private setParamsFromLocation(ctrl: IVideoController) {
            if (ctrl.$location.search().categoryId)
                ctrl.videoFilter.categoryId = parseInt(ctrl.$location.search().categoryId);
            else
                ctrl.videoFilter.categoryId = null;
            if (ctrl.$location.search().q)
                ctrl.videoFilter.q = ctrl.$location.search().q;
            else
                ctrl.videoFilter.q = null;
        }
        
        private categoriesForSelect() {
            var categories = angular.copy(this.categories);
            for (var i = 0; i < categories.length; i++) {
                categories[i].title = new Array(parseInt(<any>categories[i].depth) + 1).join("____") + categories[i].title;
            }
            return categories;
        }

    }

    angular
        .module("app.video")
        .service("app.video.videoService", VideoService);
}
