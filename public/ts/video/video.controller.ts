module app.video {
    "use strict";

    export interface IVideoController {
        videos: Array<IVideo>;
        topRated: Array<IVideo>;
        videosTotalCount: number;
        selectCategory: Array<ICategory>;
        videoFilter: IVideoFilter;
        videosLoading: boolean;
        channelData: IChannelData;
        newVideo: IVideo;
        getCategoryTitle(): void;
        
        // injecting services
        $location: ng.ILocationService;
        $scope: ng.IScope;
    }

    class VideoController implements IVideoController {

        videos: Array<IVideo> = [];
        topRated: Array<IVideo> = [];
        videosTotalCount: number = 0;
        selectCategory: Array<ICategory> =[<ICategory>{
            title: "- Выберите категорию -",
            id: 0,
            depth: 0
        }];
        videosLoading: boolean = true;
        channelData: IChannelData;
        videoFilter: IVideoFilter = {
            page: 1,
            perPage: 12,
            categoryId: 0,
            order: null,
            q: ""
        };
        newVideo: IVideo;
        videoTotalCount: number = 0;

        static $inject = ["app.video.videoService", "$scope", "$location"];

        constructor(
            private videoService: IVideoService,
            public $scope: ng.IScope,
            public $location: ng.ILocationService
        ) {

            // get video categories
            videoService.getCategories(this);

            // get youtube channel new video
            videoService.getNewVideo(this);
            
            // get youtube top rated videos
            //videoService.getTopRated(this);

            // get youtube channel data (picture, title, description, statistics)
            videoService.getChannelData(this);

            // watch for changes in videoFiler variables
            videoService.watchVideoFilter(this);

            // watch for url variables
            videoService.watchLocation(this);

        }
        
        getCategoryTitle() {
            var category = this.videoService.getCategoryById(this.videoFilter.categoryId);
            return category ? category.title : "";
        }
        
    }

    angular.module("app.video")
        .controller("app.video.VideoController", VideoController);

}